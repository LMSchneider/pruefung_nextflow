Patient 1: Doxycycline (keine Tet Resistenz gefunden, also Prio 1)

Patient 2: Chloramphenicol (Tet Resistenz, also kein Prio 1 Antibiotikum;
           APH, StrA, StrB und SAT Resistenz, also kein Prio 2 Antibiotikum;
           keine Cat_Phe Resistenz, also Prio 3 Antibiotikum)

Patient 3 : Gentamycin (Tet Resistenz, als kein Prio 1 Antibiotikum;
           keine APH, StrA, StrB und SAT Resistenz, also Prio 2 Antibiotikum)
