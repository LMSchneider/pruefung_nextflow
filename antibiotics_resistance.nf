nextflow.enable.dsl = 2

params.infile = "./rawdata/patient*.fastq"
params.outdir = "./results/"
params.gene_db = "./CARD_v3.0.8_SRST2.fasta"

process fastp {
  publishDir "${params.outdir}/fastq", mode: "copy" , overwrite: true
  container "https://depot.galaxyproject.org/singularity/fastp%3A0.22.0--h2e03b76_0"
  input:
    path infile
  output:
    path "trimm_${infile.getSimpleName()}.fastq", emit: fastqtrim
  script:
     """
    fastp -i ${infile} -o trimm_${infile.getSimpleName()}.fastq
    """
  }

process fastqc {
  publishDir "${params.outdir}/fastqc", mode: "copy" , overwrite: true
  container "https://depot.galaxyproject.org/singularity/fastqc%3A0.11.9--hdfd78af_1"
  input:
    path infile
  output:
    path "*_fastqc.html"
  script:
      """
    fastqc ${infile} 
    """
}

process srst2 {
  publishDir "${params.outdir}/srst2", mode: "copy" , overwrite: true
  container "https://depot.galaxyproject.org/singularity/srst2%3A0.2.0--py27_2"
  input:
    path infile
    path gene_db
  output:
    path "srst2__genes__CARD_v3.0.8_SRST2__results.txt"
  script:
    """
    srst2 --input_se ${infile} --output srst2 --gene_db ${gene_db}
    """
}

workflow {
inchannel = channel.fromPath(params.infile).flatten()
gene_db = channel.fromPath(params.gene_db)
trim_data = fastp(inchannel)
fastqc(trim_data.fastqtrim.collect())
srst2(trim_data.collect(),gene_db)
}
